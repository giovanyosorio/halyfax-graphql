var mongoose = require("mongoose");
var Schema = mongoose.Schema;


//var mongo = require('mongodb'); //Importar la libreria para utilizar mongodb

//var MongoClient = require('mongodb').MongoClient;



mongoose.connect('mongodb://localhost/junkies', {
  useCreateIndex: true,
  useNewUrlParser: true
})
  .then(db => console.log('DB is connected'))
  .catch(err => console.error(err));

var posibles_valores= ["M","F"];

var email_match= [/^\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/,"coloca un email valido"];


var user_schema= new Schema({

  password:{type: String,
    required: true,minlength: [8, "el password es corto"],
    validate:{
      validator: function(p){
        return this.password==p;
      } ,
      message: "las contrasenas no son iguales"
    }
  },
  email:{type: String,required: "el correo es obligatorio ",match:email_match},
  sex: {type:String, enum:{values: posibles_valores,message:"opcion no valida" }}
});


var User=mongoose.model("User", user_schema);

module.exports= User;
