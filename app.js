var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const bodyParser= require('body-parser');
//var brcypt = require('bcrypt');
var indexRouter = require("./routes/index");
var User = require("./routes/user");
var loginRouter = require("./routes/login");
var registerRouter = require('./routes/registrarse.');
var passport = require("passport");
var perfil = require('./routes/perfil');
var ciudadesRouter =require('./routes/ciudades')
//Configuracion e inicializacion de firebase
var firebase = require("firebase");
var donarRouter=require('./routes/donar');
var provider = new firebase.auth.GoogleAuthProvider();

var config = {
  apiKey: "AIzaSyBHw-PINAxmRtTXyhmtYtHGWWGkIWJqTzs",
  authDomain: "haly-c1b7d.firebaseapp.com",
  databaseURL: "https://haly-c1b7d.firebaseio.com",
  projectId: "haly-c1b7d",
  storageBucket: "haly-c1b7d.appspot.com",
  messagingSenderId: "914670521249"
};
firebase.initializeApp(config);



var app = express(); //api de express

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.get("/registrarse",function(req,res) {
        res.render("registrarse");
});


// app.post("/users", function (req,res,next){
//   var user= new User({  email : req.body.email, password :req.body.password });

// user.save().then(function(user){
//   res.send("guardamos el usuario excitosamente my friend");

// },function(err) {

//     console.log(String(err));
//     res.send("no pudimos guardar la informacio  ");

// });

// });
app.post("/register", function (req,res){
  var user = new User({

    email: req.body.email,
    password: req.body.password
  });
  user.save().then(function(us){
    console.log(req.body);
  })
  console.log(req.body);

  firebase.auth()
  .createUserWithEmailAndPassword(req.body.email, req.body.password)
  .then(function(model){
    res.send("guardamos el usuario excitosamente");
    //res.red("/login");
    //Usuario existe y llevelo
  //  console.log(model);
  }).catch(function(error){
    res.send(error);
    console.log(error);
  })
});

app.get("/login",function(req,res) {

  User.find(function(err, doc){
    console.log(doc);
        res.render("login");

});
});

app.post('/upload', async (req, res) => {
  const image = new Image();
  image.title = req.body.title;
  image.description = req.body.description;
  image.filename = req.file.filename;
  image.path = '/img/uploads/' + req.file.filename;
  image.originalname = req.file.originalname;
  image.mimetype = req.file.mimetype;
  image.size = req.file.size;

  await image.save();
  res.redirect('/');
});

app.post("/sessions", function (req,res){

  console.log(req.body);

  firebase.auth()
  .signInWithEmailAndPassword(req.body.email, req.body.password)
  .then(function(model){
  //  if ( !model ) { //No existe ese registro y no trae datos
      //Redirijir al login con error: No se encontro al usuario
  //  }
    //Usuario existe y llevelo
  //  console.log(data);
  res.redirect("/perfil");
  }).catch(function(error){
res.render("Username or password is wrong, ")  })


 });


app.use("/", indexRouter);
app.use("/user", User);
app.use("/login",loginRouter);
app.use("/perfil",perfil);
app.use('/registrarse', registerRouter);
app.use('/ciudades',ciudadesRouter)
app.use('/donar',donarRouter)
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

app.use(bodyParser.urlencoded({ extended: true }));

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};
  console.log('run on enviroment')
  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
